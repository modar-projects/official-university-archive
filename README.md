<p align="center">
  <a href="https://gitlab.com/modar-projects/official-university-archive" title="Official University Archive">
      <h1 align="center" font-size = "40px"> Official University Archive </h1> 
  </a>
</p>


## :point_right: Description

### Official  University   Archive  is  a  web  application  functions  as  archiving  system   for   university’s  official ### 

### documents   which   offers   five   main   services: Storing, classifying, adding, editing and searching for a  ###

 ### particular document or  set of documents in the most efficient way possible. ### 


<h2 align="center">🌐 Links 🌐</h2>
<p align="center">
    <a href="https://gitlab.com/modar-projects/official-university-archive" title="Official University Archive">📂 Repo</a>
    ·
   
</p>


## 🚀 Features

- **Authentication System**

- **Storing documents**

- **Categorizing  documents**

- **Quick access to documents by searching based on content or ID**

- **Editing documents**

- **Downloading documents**

- **Managing users of system by admin(Manager)**


## 📂 Screen Shots
 #### 1. Signin : Here where manager or employee can sign in into Official University Archive.

<p align="center">
  <a href="/ScreenShots/1-signin.png" title="screenShot-home">
    <img src="/ScreenShots/1-signin.png "width="80%" alt="screenShot-signin"/>
  </a>
</p>

----
 #### 2. Dashboard : Here where user can see the main categories existed in system , go to individual page to each   
  #### category and see how many documents for each category.

<p align="center">
  <a href="/ScreenShots/2-dashboard.png" title="screenShot-dashboard">
    <img src="/ScreenShots/2-dashboard.png "width="80%" alt="screenShot-signup"/>
  </a>
</p>

----
 #### 3. Category Content : Here where  user  can  browse  the  documents  of  specific category  , show  documents 
####  of  specific  subcategory  ,  search  about  documents based on ID or Content of document , go to document’s  
#### details and edit page or go to add  new document page.


<p align="center">
  <a href="/ScreenShots/4-scientific-body-docs.png" title="screenShot-categoryContent">
    <img src="/ScreenShots/4-scientific-body-docs.png "width="80%" alt="screenShot-categoryContent"/>
  </a>
</p>

----
 #### 4. Show Details and Edit : Here where user can show details of specific document and edit its data.

<p align="center">
  <a href="/ScreenShots/5-details.png" title="screenShot-details">
    <img src="/ScreenShots/5-details.png"width="80%" alt="screenShot-details"/>
  </a>
</p>

----
 #### 5. Add New Document: Here where user can add a new document..

<p align="center">
  <a href="/ScreenShots/6-add-document.png" title="screenShot-addDocument">
    <img src="/ScreenShots/6-add-document.png"width="80%" alt="screenShot-addDocument"/>
  </a>
</p>

----
 #### 6. Admin Profile : Here where admin can show ,  edit his information , add new user as admin or employee
 ### and delete existed user by his user name.


<p align="center">
  <a href="/ScreenShots/7-admin-profile.png" title="screenShot-adminProfile">
    <img src="/ScreenShots/7-admin-profile.png "width="80%" alt="screenShot-adminProfile"/>
  </a>
</p>

----
 #### 7. Employee Profile : Here where employee can just show his information. 

<p align="center">
  <a href="/ScreenShots/8-employee-profile.png" title="screenShot-employeeProfile">
    <img src="/ScreenShots/8-employee-profile.png" width="80%" alt="screenShot-employeeProfile"/>
  </a>
</p>

----


## 🦋 Prerequisite

- [MySQL](https://www.mysql.com/ "MySQL")  Installed

- [PHP +7.3](https://www.php.net/ "PHP")  Installed

- [Node.js +12.18](https://nodejs.org/en/ "NodeJS")  Installed

## 🛠️ Installation Steps

1. Clone the repository

```Bash
git clone https://gitlab.com/modar-projects/official-university-archive.git
```

2. Create new database named as 'ofa'

3. Import 'ofa.sql' file into created database

4. Change the working directory

```Bash
cd Back-end
```

5. Run the project 

```Bash
php artisan serve
```

6. Change the working directory

```Bash
cd Front-end
```

7. Run the project 

```Bash
npm start
```

8. Read 'credentials.txt' file and Signin as admin or employee

**🎇 You are Ready to Go!**

## 👷 Built with

 [MySQL](https://www.mysql.com/ "MySQL"): as Database Engine

 [Laravel](https://laravel.com/ "Laravel"): as RESTful API

 [React](https://reactjs.org/ "React"): as Front End


## 🎊 Further Approach

- [ ] Add more categories.

- [ ] Give admin ability to give permissions to employees to just access to specific categories.


## 🧑🏻 Author

**Modar Alkasem**

- 🌌 [Profile](https://gitlab.com/modarAlkasem/ "Modar Alkasem")

- 🏮 [Email](mailto:modarAlkasem@gmail.com?subject=Hi%20from%20official-university-archive "modarAlkasem@gmail.com")



